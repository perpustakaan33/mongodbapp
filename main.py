from fastapi import FastAPI
import books_route
app = FastAPI()

app.include_router(books_route.router)

@app.get("/")
async def read_main():
    return {"message": "Hello Bigger Applications!"}