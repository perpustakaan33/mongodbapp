from books_api import search_book
from fastapi import APIRouter
from books_api import *

router = APIRouter()

@router.get("/bookbyid")
async def view_search_books_id(params:dict):
    result = view_search_books_id(**params)
    return result

@router.get("/bookbyname")
async def view_search_books_name(params:dict):
    result = view_search_books_name(**params)
    return result

@router.get("/books")
async def view_search_books_by_name():
    result = search_book()
    return result